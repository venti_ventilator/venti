# Manufacture Prototype 2 3D Printed Parts

## Equipment and Equipment Qualification
1. Caliper
 - Size equal to or greater than 150mm
 - Calibrated
2. 3D Printer
 - Build volume equal to or greater than 150 by 150 by 150mm
 - Minimum layer height equal to or smaller than 0.2mm
 - Dimensional accuracy requirement:
    - Print model [cube](./cube.stl) and verify:
      - Cube X dimension is between 9.8 and 10.2mm
      - Cube Y dimension is between 14.8 and 15.2mm
      - Cube Z dimension is between 19.8 and 20.2mm
    - Print model [ring](./ring.stl) and verify:
      - Inner circle diameter is between 19.8 and 20.2mm
    - Print model [ramp](./ramp.stl) and verify:
      - Acceptable finish in the bottom of the 45 degrees surface.
    - Print model [clearance](./clearance.stl) and verify:
      - Pins two through 5 are removable

## Procedure and Verification
1. Print model [impeller](./impeller.stl)
2. Print model [electronicsTop](./electronicsTop.stl)
3. Print model [electronicsBottom](./electronicsBottom.stl)
4. Print model [coupler](./coupler.stl)
5. Print model [chamberTop](./chamberTop.stl)
6. Print model [chamberBottom](./chamberBottom.stl)
7. Print model [chamberInner](./chamberInner.stl)
8. Print model [chamberInletRight](./chamberInletRight.stl)
9. Print model [chamberInletLeft](./chamberInletLeft.stl)