# Manufacture Prototype 1 3D Printed Parts

## Equipment and Equipment Qualification
1. Caliper
 - Size equal to or greater than 150mm
 - Calibrated
2. 3D Printer
 - Build volume equal to or greater than 150 by 150 by 150mm
 - Minimum layer height equal to or smaller than 0.2mm
 - Dimensional accuracy requirement:
    - Print model [cube](./cube.stl) and verify:
      - Cube X dimension is between 9.8 and 10.2mm
      - Cube Y dimension is between 14.8 and 15.2mm
      - Cube Z dimension is between 19.8 and 20.2mm
    - Print model [ring](./ring.stl) and verify:
      - Inner circle diameter is between 19.8 and 20.2mm
    - Print model [ramp](./ramp.stl) and verify:
      - Acceptable finish in the bottom of the 45 degrees surface.
    - Print model [clearance](./clearance.stl) and verify:
      - Pins two through 5 are removable

## Procedure and Verification
1. Print model [ImpellerBal](./ImpellerBal.stl)
2. Print model [RotorBalancer](./RotorBalancer.stl)
3. Print model [case-mid-1](./case-mid-1.stl)
4. Print model [case-mid-2](./case-mid-2.stl)
5. Print model [case-drive](./case-drive.stl)
6. Print model [case-top](./case-top.stl)
7. Print model [case-bottom](./case-bottom.stl)
8. Print two copies of model [Connector](./Connector.stl)
