# Venti
Ventilator for mass production

## Overview
The goal is to develop a ventilator that can be manufactured similar to a consumer electronics good, that can be mass manufactured. We should end up with a few injection molded parts, a PCBA and a battery. But we don't know how that would look yet.

## Approach
The starting point of the project was the ventilator requirements released by [MHRA](references/minimumRequirements.pdf). From there we define our problem, create assumptions, a prototype and test it. This cycle is iterated many times as the initial problem "ventilate patient" gets translated into many smaller, more specific problems like "locate a flow sensor with minimum detection of 1ml/s and accuracy of 5%".
The problem definition is documented in the specification documents, see below, for the system and each component of the ventilator. For the testing cycles, test plans and reports are utilized.
The tools and solutions are selected in the process as a result of what is needed for a prototype and a test.

## Problem Definition Documents
 - [User Needs](/docs/UserNeeds.md)
 - [System Architecture](/docs/SystemArchitecture.md)
 - [System Specification](/docs/SystemSpecification.md)
 - [Compressor Specification & Design](/docs/CompressorSpecification.md)
 - [Flow Sensor Specification & Design](/docs/FloSensorSpecification.md)
 - Pressure Sensor Specification
 - Controller Specification
 - Control Firmware Specification
 - Interface Firmware Specification
 - Monitoring Software Specification
 ...

## Verification Testing
 - System Test Plan
 - System Test Report
 - Compressor Test Plan
 - Compressor Test Report
...

## Validation Testing
 - Venti Test Plan
 - Venti Test Report

## Compliance
 - Standards Reports 