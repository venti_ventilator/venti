from src.aluminumRing import AluminumRing

components = [AluminumRing()]

for component in components:
    component.build_doc()