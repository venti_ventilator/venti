# Glossary
The following terms and acronyms are used throughout the documentation:
 - bpm: Breath Per Minute
 - COTS: Commercial Off The Shelf
 - PEEP: Minimum pressure always keep by the ventilator to avoid collapse of airways
 - SpO2: Oxygen concentration in blood measure with optical sensor
 