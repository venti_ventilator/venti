# Flow Sensor Specification and Design

To find out the accuracy and range requirements for the flow sensor to achieve the system requirements of flow control the following steps were performed:
 - Definition of maximum flow rate from the maximum volume over the maximum rate
 - Achieve the accuracy of 10ml by:
   - Definition of minimum flow rate capable of being measurable
   - Definition of minimum acquisition rate
   - Definition of sensor accuracy

### Maximum Flow Rate
To deliver the maximum volume of 800ml in the inspiratory/expiratory ratio 1:3 at 30bpm we achieve a flow rate rate of 1600ml/s.

### Volume Accuracy
The delivered volume is calculated by integrating the flow rate measurements over time.
For the purpose of the error calculation the waveform is considered an exponential during the expiration phase, this because it is where the smallest measurements will be made and where the flow measurement error will be the most significant. The exponential is based on 250ml in the inspiratory/expiratory ration 1:1 at 10bpm or 3s. The final result error is a combination of the minimum flow measure and the error of integration and the error of each individual measurement.
```math
V = \int_{0}^{T} a.e^{T-t} = a \left( e^{T} - 1 \right)
```
```math
\hat{V} = \left\{\begin{matrix}
\frac{a.T_s}{2} \sum_{k=1}^{T/T_s-1}(x_{k-1} + x_k), if x_k >= x_min \\ 0, if x_k < x_min 
\end{matrix}\right
```
```math
e = V - hat{V}
```

## Flow Sensor Requirements
|ID|Requirement|Source/Rationale|Tests|State|
|--|--|--|--|--|
| R-FS-1 | The Flow Sensor shall reach a pressure of 800 mmH2O | R-SYS-3 | TBD | Done |
| R-FS-2 | The Compressor shall maintain maximum pressure and flow for 14 days with reliability of 0.99 | R-SYS-9 | TBD | Done |
| R-FS-3 | The Compressor shall maintain a flow of 300 ml/s at a pressure of 800 mmH2O | R-SYS-4, R-SYS-3 | TBD | Done |

## High Level Design

### Pitot Tube
Pitot tubes are capable of measuring volumetric and mass flows, nevertheless it requires the flow to have high innertial forces in relation to viscosity (Re > 3150). In the case of the ventilator [1]. The minimum flow to be measured would have a Reynolds in the order of 15 to 80 depending on the cross-section area at the sensor plane.

### Thermal Flow Sensor

### Ultrasonic Doppler

### Ultrasonic Time-of-Flight

### Differential Pressure Sensor

HAFUXX0100LXAXT
AWM720P1

## References
 - [1] [Pitot Flow Meters](https://www.paab.com/dokument/bibliotek/Flodesmatning/introduction_itabar_en.pdf)
