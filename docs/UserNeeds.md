# User Needs

## Users
The following users are considered in the development of Venti:
 - Patient
 - Physician
 - Hospital

## Needs
The state indication for each need may have the following values:
 - Pending, no work performed on it
 - Ready, defined and reviewed
 - Assigned, functionality is fully covered by children requirements
 - Done, implementation complete
 - Verified, test passing

### Performance
|ID|Need|User|Source/Rationale|Children|Test|State|
|--|--|--|--|--|--|--|
| UN-1 | Venti shall deliver a volume of 450±10ml per breath | Patient | [1] |  R-SYS-1, R-SYS-2 | TBD | Assigned |
| UN-2 | Venti shall deliver a volume of 350±10ml per breath | Patient | [1] |  R-SYS-1, R-SYS-2 | TBD | Assigned |
| UN-3 | Venti shall reach a pressure of up to 350 mmH2O | Patient | [1] | R-SYS-3 | TBD | Assigned |
| UN-4 | Venti shall maintain a minimum pressure ranging from 50 to 200 mmH20 at all times | Patient | [1] |  R-SYS-3 | TBD | Assigned |
| UN-5 | Venti shall achieve a flow rate of a minimum of 400ml per 1.5 seconds | Patient | [1] |  R-SYS-4 | TBD | Assigned |
| UN-6 | Venti shall deliver a breath rate from 10 to 30 breaths per minute in steps of 2 | Patient | [1], [3] | R-SYS-5 | TBD | Assigned |
| UN-7 | Venti shall support inspiratory to expiratory rates from 1:1 to 1:3 | Patient | [3] | R-SYS-12 | TBD | Assigned |

### Capabilities
|ID|Need|User|Source/Rationale|Children|Test|State|
|--|--|--|--|--|--|--|
| UN-7 | Venti shall ventilate a patient incapable of breathing | Patient | [1] | TBD | TBD | Pending |
| UN-8 | Venti shall concentrate oxygen in air to the range of 0.5 to 1.00 volume ratio | Patient | [1] | TBD | TBD | Pending |
| UN-9 | Venti shall sense a breath initiated by the patient and support it to achieve the desired volume | Patient | [1] | R-SYS-6 | TBD | Pending |
| UN-10 | Venti shall filter the intake air for bacteria and viruses | Patient | [1] | TBD | TBD | Pending |
| UN-11 | Venti shall be able to increase humidity in air provided to patient | Patient | [1] | TBD | TBD | Pending |
| UN-26 | Venti shall implement a mechanical fail safe at 80 mmH20 output pressure level | Patient | [3] | TBD | TBD | Ready |

### Compatibility
|ID|Need|User|Source/Rationale|Children|Test|State|
|--|--|--|--|--|--|--|
| UN-12 | Venti shall operate with oxygen rich air from 0.21 to 1.00 in volume | Patient | [1] | R-SYS-7 | TBD | Assigned |
| UN-13 | Venti shall connect to COTS 22mm female connectors for catheter | Hospital | [1] | TBD | TBD | Pending |
| UN-27 | Venti shall be compatible with autoclave sterilization | Hospital | Broadly available method | R-SYS-13 | TBD | Ready | 

### Life
|ID|Need|User|Source/Rationale|Children|Test|State|
|--|--|--|--|--|--|--|
| UN-14 | Venti shall work continuously for 14 days | Patient | [1] | R-SYS-9 | TBD | Assigned |
| UN-28 | Venti shall support 50 sterilization cycles | Hospital | Number of patients for life of ventilator | R-SYS-13 | TBD | Ready |

### User Interface
|ID|Need|User|Source/Rationale|Children|Test|State|
|--|--|--|--|--|--|--|
| UN-15 | Venti shall display the maximum pressure reached during each breath cycle in mmH2O units | Physician | [1] | TBD | TBD | Pending |
| UN-16 | Venti shall display the average pressure reached during each breath cycle in mmH2O units | Physician | [1] | TBD | TBD | Pending |
| UN-17 | Venti shall generate an alarm on pressure loss | Physician | [1] | R-SYS-10 | TBD | Pending |
| UN-18 | Venti shall generate an alarm on oxygen concentration loss | Physician | [1] | R-SYS-10 | TBD | Pending |
| UN-19 | Venti shall display the SpO2/FiO2 for the patient corrected by the altitude | Physician | [2] | R-SYS-11 | TBD | Pending |
| UN-20 | Venti shall display the volume of air per breath in ml units | Physician | [2] | TBD | TBD | Pending |
| UN-21 | Venti shall display the specific volume of air per body weight of patient in ml/kg units | Physician | [2] | TBD | TBD | Pending |
| UN-22 | Venti shall display the ventilation rate in bpm units | Physician | [2] | TBD | TBD | Pending |
| UN-23 | Venti shall display the PEEP value in mmH2O units | Physician | [2] | TBD | TBD | Pending |
| UN-24 | Venti shall allow one Physician to monitor up to 100 units simultaneously | Physician| Environment of COVID | TBD | TBD | Pending |
| UN-25 | Venti shall display percentage of breaths spontaneously taken by the patient in % | Physician | [2] | TBD | TBD | Pending |

## References
 - [1] [Frontier Tech 4 COVID ventilator challenge form](/references/frontierTech4CovidForm.pdf)
 - [2] [NIH NHLBI ARDS Clinical Network Mechanical Ventilation Protocol Summary](//references/ventilationProtocol.pdf)
 - [3] [Minimal Requirements for Rapidly Manufactured Ventilation System](/references/minimumRequirements.pdf)