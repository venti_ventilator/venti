# System Specification


## Hardware Capabilities
|ID|Requirement|Source/Rationale|Children|Test|State|
|--|--|--|--|--|--|
| R-SYS-1 | The system shall deliver a volume of breath from 250 to 800 ml in 50ml steps | UN-1, UN-2, [1], [2] | TBD | TBD | Ready |
| R-SYS-2 | The system shall deliver breath volume with an accuracy of ±10 ml | UN-1, UN-2 | TBD | TBD | Ready |
| R-SYS-3 | The system shall achieve a pressure range from 0 to 800 mmH20 with work factor of 1 | UN-3, UN-4, [1] | R-COMP-1, R-COMP-2 | TBD | Assigned |
| R-SYS-4 | The system shall deliver a minimum flow of 300ml/s for full pressure range | UN-4 rounded up | R-COMP-3 | TBD | Assigned |
| R-SYS-5 | The system shall deliver breath from of 10 to 30 bpm in steps of 2 | UN-6 | TBD | TBD | Ready |
| R-SYS-6 | The system shall measure output pressure from 0 to 500 mmH20 | UN-3, UN-4, [1] | TBD | TBD | Ready |
| R-SYS-7 | The system shall measure output pressure with an accuracy of 0.1 mmH20 | UN-9, be able to detect breath starting, 1/100th of alveolar pressure of -10mmH20 during inspiration | TBD | TBD | Ready |
| R-SYS-8 | The system components in touch with the air flow shall be compatible with 100% oxygen | UN-12 | TBD | TBD | Ready |
| R-SYS-9 | The system components shall have a combined reliability greater then 97% over the period of 14 days of continuous use | UN-14 | R-COMP-2 | TBD | Pending |
| R-SYS-10 | The system shall generate audible alert of a minimum of 80db | UN-17, UN-18, standard of sound alerts | TBD | TBD | Pending |
| R-SYS-11 | The system shall measure SpO2 of blood from 60 to 99% with resolution of 1% and accuracy of X% | UN-19 | TBD | TBD | Pending |
| R-SYS-12 | The system shall support inspiratory to expiratory rates from 1:1 to 1:3 | UN-6 | TBD | TBD | Pending |
| R-SYS-13 | The system shall be sterilized with autoclave 50 times while maintaining acceptable performance | UN-27, UN-28 | TBD | Ready |


## Compliance
The following standards are applicable to Venti:
 - ISO 10651-3:1997 - Lung ventilators for medical use — Part 3: Particular requirements for emergency and transport ventilators
 - IEC 60601-1+AMD1:2012 - Medical Electrical Equipment
 - IEC 60601-1-8:2006/DAMD 2 - Medical electrical equipment — Part 1-8: General requirements for basic safety and essential performance — Collateral standard: General requirements, tests and guidance for alarm systems in medical electrical equipment and medical electrical systems
 - IEC 60601-1-10:2007/DAMD 2 - Medical electrical equipment — Part 1-10: General requirements for basic safety and essential performance — Collateral standard: Requirements for the development of physiologic closed-loop controllers
 - IEC 60601-1-12:2015/DAMD 1 - Medical Electrical Equipment — Part 1-12: General requirements for basic safety and essential performance - Collateral Standard: Requirements for medical electrical equipment and medical electrical systems used in the emergency medical services environment
 - ISO 80601-2-12:2020 - Medical electrical equipment — Part 2-12: Particular requirements for basic safety and essential performance of critical care ventilators
 - ISO 80601-2-61:2017 - Medical electrical equipment — Part 2-61: Particular requirements for basic safety and essential performance of pulse oximeter equipment
 - ISO 80601-2-69:2014 - Medical electrical equipment — Part 2-69: Particular requirements for basic safety and essential performance of oxygen concentrator equipment
 - ISO 80601-2-74:2017 - Medical electrical equipment — Part 2-74: Particular requirements for basic safety and essential performance of respiratory humidifying equipment
 - ISO 18562-1:2017 - Biocompatibility evaluation of breathing gas pathways in healthcare applications — Part 1: Evaluation and testing within a risk management process


## References
 - [1] Specification of Existing Ventilator. Available at https://www.usa.philips.com/healthcare/product/HC1054260/trilogy100-portable-ventilator/specifications.

 - [2] [Minimal Requirements for Rapidly Manufactured Ventilation System](/references/minimumRequirements.pdf)