## Inlet
The inlet connectors must be designed and tested as per ISO 5356-1:2015 [1].



# References
[1] ISO 5356-1:2015. Available at https://www.iso.org/obp/ui#iso:std:iso:5356:-1:ed-4:v1:en.