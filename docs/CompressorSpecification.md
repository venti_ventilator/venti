# Compressor Specification and Design

## Specification

|ID|Requirement|Source/Rationale|Tests|State|
|--|--|--|--|--|
| R-COMP-1 | The Compressor shall reach a pressure of 800 mmH2O | R-SYS-3 | TBD | Done |
| R-COMP-2 | The Compressor shall maintain maximum pressure and flow for 14 days with reliability of 0.99 | R-SYS-9 | TBD | Done |
| R-COMP-3 | The Compressor shall maintain a flow of 300 ml/s at a pressure of 800 mmH2O | R-SYS-4, R-SYS-3 | TBD | Done |
| R-COMP-4 | The Compressor shall be sterilizable with autoclave. | R-SYS-13 | TBD | Done |

## Design
To achieve a high flow rate of 3e-4 m<sup>3</sup>/s at a compact size and low pressure increase of 7846 Pa a centrifugal compressor design is adopted. This design is commonly adopted in the industry of both ventilators and CPAP machines.
The expected impact of the compressor is to increase the pressure from 1 to 2 by 7846 Pa for a flow of 300 ml/s, given the mechanical work W provided by an electric motor, Figure 1.
![Figure 1 - Compressor Overview](/docs/img/compressorOverall.jpg)

The relevant parameters that we want to correlate are given below.
 - q [m<sup>3</sup>/s]: fluid volumetric flow
 - p<sub>x</sub> [Pa]: absolute pressure at 1 and 2
 - T<sub>x</sub> [K]: absolute temperature at 1 and 2
 - W [W]: mechanical work added to the fluid
 - ω [rad/s]: angular speed of compressor

### Material
The impeller, as all the parts of this ventilator, are intended to be 3D printed for prototyping and injection molded for production. The material also needs to be biocompatible and survive several sterilization cycles in an autoclave.
Given the different alternatives evaluated below Polycarbonate was chosen for its, cost, good compatibility with steam sterilization and chemical resistance to oxygen.

|Material|Tensile Strength MPa|[Autoclave Performance](https://www.industrialspec.com/resources/plastics-sterilization-compatibility/)|3D Printable?|Moldable?|
|--|--|--|--|--|
|Polylactic Acid (PLA)|45|Poor|Y|Y|
|Polycarbonate (PC)|68|Good|Y|Y|
|Polyethylene Terepthalate Glycol-Modified (PETG)|53|Poor|Y|Y|
|Nylon 12|48|Poor|Y|Y|
|Acrylonitrile Butadiene Styrene (ABS)|45|Poor|Y|Y|
|Polyetherimide (PEI)|105|Fair|Y|Y|
|Polyethylene (PE)|33|Poor|Y|Y|
|Polypropylene (PP)|36|Good|Y|Y|
|Polyetheretherketone (PEEK)|105|Good|Y|Y|

### Input & Output
Both input and output of ventilator/compressor are a standard 22mm respiratory equipment connector [9]. The cone taper is of 1:40 and the following dimensions are used:

|Location|Dimension (mm)|+Tolerance (mm)|-Tolerance (mm)|
|--|--|--|--|
|Taper Length|19.5|0.5|0.0|
|Recess Diameter|20|0.5|0.0|
|Diameter at chord|22.37|0.04|0.04|
|Chord|15|n/a|n/a|


### Compression Process Description
The centrifugal compressor increases the pressure of the fluid by adding kinetic energy with a rapidly rotating impeller and the converting this energy to potential.

The compression process can be considered isenthalpic with no heat exchange due the quick period in which the air flows through the compressor and work is added to the fluid. With no losses, the power required for given increase in stagnation pressure can be estimated by the isenthalpic pressure-temperature relation and the difference in stagnation enthalpy between the input and the output for a given amount of work.
```math
\frac{p_{t_o}}{p_{t_i}} = {\left( \frac{T_{t_o}^{ideal}}{T_{t_i}} \right)}^{\frac{\gamma}{\gamma - 1}}
```
```math
W^ideal = \dot{m} (h_{t_o}^{ideal} - h_{t_i}) \approx \dot{m} c_p (T_{t_o}^{ideal} - T_{t_i})
```

In reality, recirculation, viscosity, shock and other losses will require more power to increase the pressure to the required specification. By adding an isentropic coefficient to the calculations we can come back after a few bench tests and adjust the model to better fit the compressor design.
```math
\eta_c = \frac{W^{ideal}}{W} = \frac{h_{t_o}^{ideal} - h_{t_i}}{h_{t_o} - h_{t_i}} \approx \frac{T_{t_o}^{ideal}-T_{t_i}}{T_{t_o}-T_{t_i}
```
```math
\frac{p_{t_o}}{p_{t_i}} = {\left[ 1 + \frac{\eta_c (T_{t_o}-T_{t_i})}{T_{t_i}} \right]}^{\frac{\gamma}{\gamma - 1}}
```

The angular speed of the impeller is another key characteristic of the compressor specially since we are trying to avoid high accuracy manufacturing processes for the low cost solution. From the conservation of angular momentum the Euler turbine equation relates the increase of stagnation temperature with the rotation of the impeller. It is assumed that the velocity of the outer diameter is much greater than the velocity of the inner diameter of the impeller (about two orders of magnitude).
```math
W =  \dot{m} \omega(r_o v_o - r_i v_i) \approx \dot{m} \omega \frac{d_i}{2} v_o \approx \dot{m} c_p(T_{t_o} - T_{t_i}) 
```
With sigma being the slippage factor between the impeller and the air flowing through it: 
```math
v_o = \omega . \frac{d_i}{2} (\omega . \frac{d_i}{2} . \sigma)
```

A compressor [python object](/src/compressor/main.py) was created to solve these equations and utilized here to create a reference plot for our compressor design.
![compressor Pressure Omega](compressorPressureOmega.png)

### Preliminary Validation of the Model (CFD)
By using an awesome UI for OpenFOAM called [SIMSCALE](https://www.simscale.com) a few simulations of an initial compressor design were evaluated.
The first main aspect tested was the beta2 angle. The initial choice of forward curved vanes was shown to have a lower overall efficiency than straight and backwards curved vanes. 
The unproven hypothesis as to why this is has to do with the fluid type and radial velocity at the impeller. As per pictures below, one can see the increased radial velocity with the backward curved vane. The geometry of the vane itself push the air towards the exit, reducing recirculation. The effect was exacerbated by the compact design of the compressor where the diffuser and collector are located below the impeller and not radially outside of it.
A decision was made to proceed the design with a beta2 of 66 degrees in backward curved configuration. Nevertheless, my knowledge is this is little and recommendations are welcome.

You may access the simulations results [here](https://www.simscale.com/projects/kilmart/venti_compressor_2/).
![Simulation Radial Speed](/docs/img/simulationRadialSpeed.png)
![Simulation Pressure Increase](/docs/img/simulationPressureIncrease.png)
![Simulation Radial Pressure](/docs/img/simulationRadialPressure.png)

### Motor Choice
Highly controllable motors are available today in the drone industry. Given the similar nature of the problem, where fine control for closed loop algorithms, triphase brushless motors are used.
The specification of these motors usually consists of a Kv [rpm/v] constant, max power P<sub>max</sum> [W] and max current I<sub>max</sum>.
We are looking for a motor with a power greater then 3.84W with a safety factor of at least 2, so > 7.68 W.
For the voltage we can assume a 1 or 2 cells lithium battery, capable of sustaining a voltage of 3.6 or 7.2V at 0.5C discharge rate.
To estimate a Kv constant range from which to select our motor, the formula for an electric motor torque is used, in which T and ω are known; and V and R are bound, allowing us to calculate the k<sub>t</sub> constant.
```math
T = \frac{(V-\omega.k_t)}{R} . k_t
```
```math
k_t [Nm/A] = \frac{8.3}{Kv [RPM/V]}
```
Assumptions:
 - T of 2.21e-3 Nm, calculated torque with a safety factor of 2
 - ω of 3465.76 rad/s
 - V of 3.6 or 7.2 V
 - R of 0.2 to 0.6 Ω, acceptable range for coil resistance
The range of Kv we are looking is from 4100 to 9200 RPM/V.

To summarize, we are looking for a motor with the following characteristics:
 - Power > 7.68W;
 - Resistance within 0.2 to 0.6 ohms;
 - Voltage of 3.6 or 7.2V (1 or 2 cells);
 - Kv within 

Here are a few options:

|ID|Power[W]|Kv[RPM]|I<sub>max</sub>[A]|V<sub>max</sub>|link|
|--|--------|-------|------------------|-----|----|
|A |9.6     |5750   |1.2               |7.4   |[goto](https://hobbyking.com/en_us/bl-2030-16-brushless-inrunner-motor-5800kv.html?queryID=36b0b986325c511a706002261a6e61fd&objectID=26786&indexName=hbk_live_magento_en_us_products)|
|B |~40     |8000   |5.46              |12   |[goto](https://www.getfpv.com/hglrc-forward-fd1103-brushless-motor-8000kv-10000kv.html)|
|C |80      |9500   |10                |7.4  |[goto](https://www.scorpionsystem.com/catalog/car/motors_2/mini-z/Z_1410_9500/)|
|D |130     |5300   |18                |7.4  |[goto](https://www.scorpionsystem.com/catalog/helicopter/motors_4/hkii-22/HK-2206-5300KV/)|
|E |45      |6100   |?                 |7.4 |[goto](https://hackermotorusa.com/shop/hacker-brushless-motors/inrunners/e10-26l/)|
|F |45      |4300   |?                 |7.4  |[goto](https://hackermotorusa.com/shop/hacker-brushless-motors/inrunners/e10-36l/)|
|G |35      |4200   |?                 |7.4  |[goto](https://hackermotorusa.com/shop/hacker-brushless-motors/outrunners/a05-10s/)|
|H |270     |4200   |22                |12   |[goto](https://www.alibaba.com/product-detail/RC-hobby-2627-3500KV-4200KV-brushless_62346905272.html?spm=a2700.galleryofferlist.0.0.64511073TnwO0H)|

Refer to [python src](/src/compressor/main.py) for the selection algorithm where an objective function is minimized resulting in motor H due to the acceptable current and voltage at maximum compressor power. Also, motor H is high availability and has an acceptable mounting configuration.

### Other Considerations
#### Diameter vs Speed of The Impeller
Based on the euler compression equation (conservation of angular momentum), the rotation of the impeller can be associated with the rise in temperature of the fluid.
```math
c_p2.T_2 - c_p1.T1 = \omega . r . V_t = \omega . r (\omega . r . \sigma)
```
Parameters:
 - c<sub>p2</sub> J/kg.K: specific heat capacity of air
 - c<sub>p1</sub> J/kg.K: specific heat capacity of air
 - ω rad/s: angular velocity of impeller
 - V<sub>t</sub>: tangential velocity of fluid at edge of impeller
 - σ: compressor slip factor between impeller velocity and fluid velocity of 0.9
Since the impeller will be constructed by a 3D printer without great accuracy and limited there is a limit to the angular speed of the shaft and the size. 
The diameter should be kept under 65mm to fit into small SLA printers and the rotational speed under 55 000 RPM for materials with Tensile Strength of 80 MPa.
The possible combinations are, therefore:
 - ω from 30550 to 55000 RPM
 - d from 36 to 65 mm
To prioritize longevity over size the initial choice is:
 - d of 64mm
 - ω of 31038 RPM or 3250.34 rad/s

#### Diameter of Impeller Root/Shaft
Since the torque is small, the angular speed is high and the manufacturing process by 3D printing is not accurate the main aspect driving shaft failure is fatigue due to imbalance.
Fatigue in polymers have two modes, thermal and mechanical. Since polymers are a poor heat conductor and absorb heat well the continues operation of an imbalanced impeller may increase heat and degrade the mechanical properties of the material.
Additionally, the literature does not offer consistent theoretical methods for estimating fatigue for 3D printed polymers at high frequency loading.
A few of the findings were:
 - Additive 3D printed polymers offer similar behavior fatigue wise to other process of manufacturing [7]
 - Polymer impellers have a stress concentration region in the impeller root edge, as per picture below [8]
 ![ImpellerStressConcentration](/docs/img/impellerStress.png)
Further considerations of design and tests will impact the diameter of the shaft, for now the following values are assumed:
 - d [mm], diameter of impeller of 60 mm
 - d<sub>e</sub>, diameter of impeller eye of 16 mm
 - d<sub>r</sub>, diameter of impeller root of 8 
 
#### Number of Vanes
To estimate the number of vanes the Wiesner correlation is used with a slip factor of 0.9. A combination of 16 vanes and 66 degrees angle from radial direction outlet was chosen.
```math
\sigma_s = 1 - \frac{\sqrt{cos{beta_2}}}{Z^0.7}
```

#### Motor to Impeller Coupling
From the CFD simulations the main forces acting on the impeller are normal to the impeller plane of 7.7N and torque of 6.3e-3 N.m. To be confirmed with bench test.

#### Oxygen Leak to Electronics
TBD, the idea is to have air forced into the system quicker than the dispersion speed of oxygen.


### Prototype 1
Consists of a 3D printed chamber with a support for the motor and impeller as well as directing ducts for the air to flow in. The prototype was also a stage to try different solutions for coupling and mounting options.
The prototype resulted in pressure ratios between input and output of about 1/10th of the desired value. This was attributed to the impeller design, and non-sealed construction joints. Several different types of impeller and chamber design were 3D printed to evaluate the aspects of the design that would drive the performance to the desired range.
A video of the prototype is available at YouTube [here](https://www.youtube.com/watch?v=v7klrKTRh20)
The components are available at the locations below:
 - [Fusion360 CAD](https://a360.co/2ZHS2AC)
 - [Particle Photon Firmware](/src/firmware/compressorTest)
 - React User Interface PWA
 - [Stl Files](/src/processes/manufacture-p1-3dprint)

![First Prototype CAD](firstPrototypeCAD.png)
![First Prototype Setup](firstPrototypeSetup.png)

### Prototype 2
The second prototype improves both on the compressor performance and overall design for a unit that can be manufactured with injection moulding and may have the components in contact with patient air separated from the electronics and sterilized.
The prototype is currently under construction, links pending for the components:
 - [Fusion360 CAD](https://a360.co/2VUPzBw)
 - Particle Photon Firmware
 - RaspberryPi Image
 - Stl Files
 - Simulation CAD

![Second Prototype CAD 1](secondPrototypeCAD1.png)
![Second Prototype CAD 2](secondPrototypeCAD2.png)
![Second Prototype CAD 3](secondPrototypeCAD3.png)


## References
 - [1] [Adiabatic process for an ideal gas](https://phys.libretexts.org/Bookshelves/University_Physics/Book%3A_University_Physics_(OpenStax)/Map%3A_University_Physics_II_-_Thermodynamics%2C_Electricity%2C_and_Magnetism_(OpenStax)/03%3A_The_First_Law_of_Thermodynamics/3.07%3A_Adiabatic_Processes_for_an_Ideal_Gas)
 - [2] [Euler Turbine Equation](https://web.mit.edu/16.unified/www/SPRING/thermodynamics/notes/node91.html)
 - [3] [Design of Centrifugal Compressor](http://164.100.133.129:81/econtent/Uploads/07PT12-CentriComp-1%2060%20%5BRead-Only%5D%20%5BCompatibility%20Mode%5D.pdf)
 - [4] [Brushless Motor Constant Estimation](https://things-in-motion.blogspot.com/2018/12/how-to-estimate-torque-of-bldc-pmsm.html)
 - [5] [Torque Equation for DC Motors](https://www.motioncontroltips.com/torque-equation/)
 - [6] [Failure Analysis of a Respirator Impeller](https://www.sciencedirect.com/science/article/pii/S2213290215000061)
 - [7] [Tensile and Fatigue Behavior of Additive Manufactured Polylactide](https://www.liebertpub.com/doi/10.1089/3dp.2017.0154)
 - [8] [Failure analysis of a polymer centrifugal impeller](https://www.sciencedirect.com/science/article/pii/S2213290215000061)
 - [9] [ISO 5356-1:2015](https://www.iso.org/obp/ui#iso:std:iso:5356:-1:ed-4:v1:en)
 