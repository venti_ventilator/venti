# System Architecture
Venti is broken down into two parts to fulfill the different aspects of the functionally needed. The first, Ventilation Unit, controls breath rate, volume and pressure. The second, Conditioning Unit, controls the humidification and temperature of the air for the patient.

The units are separated to support both invasive and non-invasive ventilation methods nad provide more flexibility to the medical team. For example, when using only non-invasive ventilation one do not need the conditioning unit with the associated cost. 

Below are two diagrams representing how each unit was broken down into components. Each component is defined in a specification and tested separately prior to integration.

Both units are able to communicate with a a server to allow up to 100 units to be monitored simultaneously for status and alerts by the hospital team.
![Two Units Sketch](/docs/img/unitsSketch.jpg)

## Ventilation Unit
One important characteristic of the ventilation unit is the separation between components in contact with the airway and components without contact. The separation allows for easy sterilization of the exposed part with an autoclave, while the sensitive electronics components are safely arranged in the other have that does not require sterilization, only disinfection.
![Ventilation Unit Overview](/docs/img/ventilationUnitOverview.png)

### Components
 - One Way Valve
 - Compressor
 - Motor
 - Flow Sensor
 - Safety Valve
 - Environment Sensor (p, T, RH and pO2)
 - Display
 - Controller
 - Battery

## Conditioning Unit
![Conditioning Unit Overview](/docs/img/conditioningUnitOverview.png)

### Components
 - One Way Valve
 - Filter
 - Reservoir
 - Humidifier
 - Heater
 - Environment Sensor (p, T, RH and pO2)
 - Display
 - Controller
 - Battery