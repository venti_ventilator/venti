### Flow Sensor Design Calculations
### variables without unit definitions are in SI units

# Imports
from math import e, sqrt, pi
from functools import reduce

# System Requirements Input
max_volume = 800e-6 # maximum volume measure by the sensor
max_rate = 30 / 60 # maximum respiratory rate
min_rate = 10 / 60 # minimum respiratory rate
acc = 10e-6 # required accuracy of volume measured
min_ie_ratio = 0.25 # minimum inspiratory/expiratory ratio
max_ie_ratio = 0.5 # maximum inspiratory/expiratory ratio

# Flow Sensor Requirements
min_flow = 5e-6 # minimum detectable flow
sampling_rate = 1000 # sampling rate
sensor_error_percentage = 5 # single measurement uncertainty

# Constants and Assumptions
test_volumes = [200e-6, 250e-6, 400e-6, 800e-6, 900e-6] # volumes for which to the error should be below maximum allowed
min_cross_section_diameter = 5e-3 # minimum practical cross section diameter where the flow sensor is installed
max_cross_section_diameter = 25e-3 # maximum practical cross section diameter where the flow sensor is installed
nu = 1.6e-5 # approximate kinematic viscosity of air/oxygen at 30deg C in m2/s

## Maximum Flow
print("** Max Flow")
t_f = min_ie_ratio / max_rate
max_flow = max_volume / t_f
print("The max flow rate is {} ml/s".format(max_flow*1e6))

## Error Estimation
print("** Error Estimation")
print(f"Min flow rate {min_flow*1e6} ml/s")
t_f = max_ie_ratio / min_rate
for vol in test_volumes:
    alpha = -vol / (-1 + t_f/e**t_f + 1/e**t_f)
    f_volume = lambda t : -alpha * (-1 + t/e**t_f + 1/e**t)
    f_flow = lambda t : alpha * (e**-t - 1/e**t_f)
    integrate = lambda series, step: step*(reduce(lambda a,b : a+b, series[1:-1], 0.0) + series[0]/2 + series[-1]/2)
    N = int(t_f * sampling_rate + 1)
    t_series = [n/sampling_rate for n in range(N)]
    hat_flow = [f_flow(t) if f_flow(t) >= min_flow else 0 for t in t_series]
    hat_vol = integrate(hat_flow, 1/sampling_rate)
    error = hat_vol - vol
    uncertainty = sensor_error_percentage * sqrt(N) * 2 / sampling_rate
    total_error = abs(error) + uncertainty/100*hat_vol
    print("For a volume of {} ml the error is {} ml".format(vol*1e6, total_error*1e6))

## Reynolds Range Estimation
print("** Reynolds Estimation")
re_sm_lo = min_flow / nu / pi / min_cross_section_diameter * 4
re_lg_lo = min_flow / nu / pi / max_cross_section_diameter * 4
re_sm_hi = max_flow / nu / pi / min_cross_section_diameter * 4
re_lg_hi = max_flow / nu / pi / max_cross_section_diameter * 4
print(f"Minimum reynolds varies between {re_sm_lo} and {re_lg_lo}")
print(f"Maximum reynolds varies between {re_sm_hi} and {re_lg_hi}")

