# compressorTest

A Particle project to control the compressor test fixture for respiratory rate and respiratory ratio.

## Design

The Particle Photon is used to control the ESC and implements a RESTApi server from which a webapp can read and configure the two properties for the respiratory rate and ratio.

The respiratory rate can vary from 10 to 60 breaths per minute.
The inspiration to expiration time ratio can vary from 1:1 to 1:3.

The API is implemented accordingly to the following spec:
```yaml
openapi: 3.0.0
info:
  title: CompressorTest API
  description: An API to control and monitor the ventilator compressor hardware.
  version: 0.1.0
paths:
  /parameters:
    get:
      summary: Returns the current parameters for the ventilator
      responses:
        '200':
          description: Successful request
          content:
            application/json:
              schema:
                type: object
                properties:
                  is_enabled:
                    type: boolean
                  r_rate:
                    type: integer
                  r_ratio:
                    type: number
    post:
      summary: Sets parameters of the ventilator
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                r_rate:
                  type: integer
                  minimum: 10
                  maximum: 60
                  required: true
                r_ratio:
                  type: number
                  minimum: 1.0
                  maximum: 3.0
                  required: true
      responses:
        '200':
          description: Parameters set
          content:
            application/json:
              schema:
                type: object
                properties:
                  r_rate:
                    type: integer
                  r_ratio:
                    type: number
        '400':
          description: Invalid Request
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                   type: string
  /start:
    get:
      summary: Starts the ventilator
      responses:
        '200':
          description: Successful request
  /stop:
    get:
      summary: Stops the ventilator
      responses:
        '200':
          description: Successful request
```

## Compiling your project
Use particle.io extension for VS Code to compile the code and flash the Particle Photon.
