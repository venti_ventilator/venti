/*
 * Library MicroRest
 * Description: very crude HTTP Rest API implementation, not for production.
 * Author: Thomas Kilmar
 * Date: 02 May 2020
 * 
 * State Machine for Request
 *  - 0, parsing method
 *  - 1, parsing url
 *  - 2, parsing header
 *  - 3, parsing body
 *  - 4, consume line
 * 
 * State Machine for Response
 *  - 0, writing protocol version
 *  - 1, writing status code
 *  - 2, writing status msg
 *  - 3, writing content type header
 *  - 4, writing content data
 *  - 5, writing line feed, ready to exit
 *  - 6, done
 *  - 7, writing separation between header and body
 *  - 8, writing other headers
 */ 

#include "micro_rest.h"

// get json integers by key
int Request::jsonInt(const char *key) {
    int value = 0;
    const char *index = strstr(body, key);
    while (*index != ':' && *index != '\0') index++;
    index++;
    while (*index != ',' && *index != '\0') {
        value = value*10 + *index - ENCODING_DIGIT_OFFSET;
        index++;
    }
    return value;
}

// get json floats by key
float Request::jsonFloat(const char *key) {
    float value = 0;
    float decade = 10;
    const char *index = strstr(body, key);
    while (*index != ':' && *index != '\0') index++;
    index++;
    while (*index != '.' && *index != '\0') {
        value = value*10 + *index - ENCODING_DIGIT_OFFSET;
        index++;
    }
    index++;
    while (*index >= (0+ENCODING_DIGIT_OFFSET) && *index <= (9+ENCODING_DIGIT_OFFSET)) {
        value = value + (*index - ENCODING_DIGIT_OFFSET)/decade;
        decade *= 10;
        index++;
    }
    return value;
}

// parse a char from the raw request into the object
void Request::nextByte(const char nbyte) {
    // cap requests at max size
    size++;
    if(size > MAX_REQUEST_SIZE) return;
    // method goes up to the first space
    if(state == 0) {
        if(nbyte == ' ') {
            state = 1;
            return;
        }
        strncat(method, &nbyte, 1);
        return;
    }
    // url goes up to the second space
    else if(state == 1) {
        if(nbyte == ' ') {
            state = 4;
            nextState = 2;
            return;
        }
        strncat(url, &nbyte, 1);
        return;
    }
    // parse headers
    else if(state == 2) {
        // no headers are parsed for now, skip to body
        if(nbyte == '\r') {
            state = 4;
            nextState = 3;
            return;
        } else {
            state = 4;
            nextState = 2;
            return;
        }
    }
    // parse body
    else if(state == 3) {
        strncat(body, &nbyte, 1);
    }
    // consume line
    else if(state == 4) {
        if(nbyte == '\n') {
            state = nextState;
            return;
        }
    }
}

// set the response body
// void Response::body(char *resBody, ) { data = resBody; }

// set the response status
void Response::status(int resCode, const char resCodeMsg[]) { 
    sprintf(codeNumber, "%d", resCode); 
    codeMsg = resCodeMsg;
}

// are bytes still available to be sent?
bool Response::available(){
    return state != 6;
}
    
// get next byte to be sent
char Response::nextByte() {
    size++;
    if (size + 2 > MAX_RESPONSE_SIZE) state = 6;
    if (crlf) {
        crlf = false;
        if(state == 5) state = 6;
        return '\n';
    }
    if (*current == '\0') {
        // writting header
        if (state == 0) {
            current = &codeNumber[0];
            state = 1;
        // writting error code
        } else if (state == 1) {
            current = &codeMsg[0];
            state = 2;
            return ' ';
        // writting error msg
        } else if(state == 2) {
            current = &headers[0];
            state = 8;
            crlf = true;
            return '\r';
        // writting other headers
        } else if(state == 8) {
            // if has body, write content type header
            if (strlen(data) > 0){
                current = &contentType[0];
                state = 3;
            // otherwise we are done
            } else state = 5;
            crlf = true;
            return '\r';
        // writting content type header
        } else if(state == 3) {
            state = 7;
            crlf = true;
            return '\r';
        // write body separation
        } else if(state == 7) {
            current = data;
            state = 4;
            crlf = true;
            return '\r';
        // writting content body
        } else if(state == 4) {
            state = 5;
            crlf = true;
            return '\r';
        }
        // if something went wrong stop
        else state = 6;
    }
    const char a = *current;
    current++;
    return a;
}

void Response::addHeader(const char header[], const char value[]){
    if(multipleHeaders) strcat(headers, "\r\n");
    strcat(headers, header);
    strcat(headers, ": ");
    strcat(headers, value);
    multipleHeaders = true;
}

// add a handler function to the server
void MicroRest::use(const char uri[], const char method[], reqHandler h) {
    if(nHandlers < MAX_HANDLE_FUNCTION) {
        handlersList[nHandlers].uri = uri;
        handlersList[nHandlers].method = method;
        handlersList[nHandlers].handler = h;
        nHandlers++;
    }
}

// main function to handle requests
void MicroRest::next() {
    // look for clients
    if (client.connected()) {
        Request req;
        Response res;
        // Serial.print("Request:");
        char a;
        delay(1);
        // get request from client
        while (int n = client.available()) {
            // Serial.print("n:");
            // Serial.println(n);
            while (n > 0) {
                a = client.read();
                // Serial.print(a);
                req.nextByte(a);
                n--;
            }
        }

        // find the first processing function with a matching url
        for(int i = 0; i < nHandlers; i++) {
            if(
                // both method and path match
                (strcmp(handlersList[i].uri, req.url) == 0 && strcmp(handlersList[i].method, req.method) == 0) ||
                // any path specified for specific method
                (strcmp(handlersList[i].uri, "*") == 0 && strcmp(handlersList[i].method, req.method) == 0)
            ){
                handlersList[i].handler(&req, &res);
                break;
            }
        }

        // if CORS is allowed respond to preflight request and add header to simple CORS
        if(isCorsAllowed) {
            res.addHeader("Access-Control-Allow-Origin", CorsOrigin);
            if (strcmp(req.method, "OPTIONS") == 0) {
                res.addHeader("Access-Control-Allow-Headers", "Content-Type");
                res.status(204, "No Content");
            }
        }
        Serial.println("Response:");
        // send response and clean up
        while(res.available()) {
            a = res.nextByte();
            // Serial.print(a);
            client.write(a);
        }
        client.flush();
        delay(100);
        client.stop();
    } else {
        client = available();
    }
}

void MicroRest::allowCors(const char corsStr[]){
    isCorsAllowed = true;
    CorsOrigin = corsStr;
}