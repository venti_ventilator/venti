/*
 * Library MicroRest
 * Description: very crude HTTP Rest API implementation, not for production.
 * Author: Thomas Kilmar
 * Date: 02 May 2020
 */

#include "string.h"
#include "stdio.h"
#include "Particle.h"

#define MAX_REQUEST_SIZE 1024
#define MAX_RESPONSE_SIZE 256
#define MAX_URL_SIZE 48
#define MAX_CONTENT_TYPE_SIZE 128
#define MAX_BODY_SIZE 128
#define MAX_METHOD_SIZE 8
#define MAX_HANDLE_FUNCTION 48

#define ENCODING_DIGIT_OFFSET 48

class Request {
  public:
    char method[MAX_METHOD_SIZE] = {'\0'};
    char url[MAX_URL_SIZE] = {'\0'};
    char body[MAX_BODY_SIZE] = {'\0'};
  private:
    int content_size = 0;
    int state = 0;
    int nextState = 0;
    int size = 0;

  public:
    int jsonInt(const char *key);
    float jsonFloat(const char *key);
    void nextByte(const char nbyte);
};

class Response {
  private:
    int state = 0;
    int size = 0;
    const char protocol[10] = "HTTP/1.1 ";
    const char contentType[63] = "Content-Type: application/json";
    const char defaultErrorMsg[10] = "Not Found";
    char headers[128] = "";
    bool multipleHeaders = false;
    bool crlf = false;
    const char *current = &protocol[0];
    const char *codeMsg = &defaultErrorMsg[0];
    char codeNumber[4] = "404";

  public:
    char data[MAX_BODY_SIZE] = {'\0'};
    // void body(char *resBody);
    void status(int resCode, const char resCodeMsg[]);
    bool available();
    char nextByte();
    void addHeader(const char header[], const char value[]);
};

typedef void (*reqHandler)(Request *, Response *);

class Handler {
  public:
    void (*handler)(Request*, Response*);
    const char *method;
    const char *uri;
};

class MicroRest : public TCPServer {
  public:
    MicroRest(const unsigned int port) : TCPServer(port) {}
    void use(const char uri[], const char method[], reqHandler h);
    void next();
    void allowCors(const char corsStr[]);

  private:
    Handler handlersList[MAX_HANDLE_FUNCTION];
    TCPClient client;
    int nHandlers = 0;
    bool isCorsAllowed = false;
    const char *CorsOrigin;
};