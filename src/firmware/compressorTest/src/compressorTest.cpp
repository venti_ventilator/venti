/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/Users/kilmar/Documents/Projects/onebreath/src/firmware/compressorTest/src/compressorTest.ino"
/*
 * Project compressorTest
 * Description: controls the compressor ESC and implements a REST Api server for configuring it.
 * Author: Thomas Kilmar
 * Date: 02 May 2020
 * 
 * RESTApi
 *  - /parameters (GET, POST)
 *  - /start
 *  - /stop
 * 
 * State Machine
 *  - 0, disabled
 *  - 1, enabled
 *    - 11, inspiration
 *    - 21, expiration
 */

// Dependencies
#include "micro_rest.h"

// Pins Configuration
void blinkIP(IPAddress localIP, int repeat);
bool isEnabled();
void stepStateMachine();
void stop();
void start();
void update_parameters(int *new_r_rate, float *new_r_ratio);
void parametersGet(Request *req, Response *res);
void parametersPost(Request *req, Response *res);
void startGet(Request *req, Response *res);
void stopGet(Request *req, Response *res);
void setup();
void loop();
#line 23 "/Users/kilmar/Documents/Projects/onebreath/src/firmware/compressorTest/src/compressorTest.ino"
#define LED_PIN D7
#define COMPRESSOR_PIN D0

// Constants
#define PORT 80
#define MAX_R_RATE 60
#define MIN_R_RATE 10
#define MAX_R_RATIO 3.0
#define MIN_R_RATIO 1.0
#define OFF_ESC_PW 900
#define ON_ESC_PW 1130

// Global Ventilator Parameters
int r_rate = 10;
float r_ratio = 2.0;

// Global variables
MicroRest api(PORT);
Servo compressor;
volatile int state = 0;
volatile int counter = 0;
volatile int overflow = 0;
volatile int ie_transition = 0;

// Support Functions
void blinkIP(IPAddress localIP, int repeat) {
  for(int j = repeat; j>0; j--) {
    for(int i = localIP[3]; i>0; i--) {
      digitalWrite(LED_PIN, HIGH);
      delay(100);
      digitalWrite(LED_PIN, LOW);
      delay(400);
    }
    delay(2000);
  }
}
bool isEnabled() {
  return (state % 10) == 1;
}

// Configure State Machine
void stepStateMachine() {
  if (isEnabled()) {
    counter++;

    // respiratory cycle counter
    if (counter > overflow) counter = 0;
    
    // on states
    if(state == 11) compressor.writeMicroseconds(ON_ESC_PW);
    else if(state == 21) compressor.writeMicroseconds(OFF_ESC_PW);

    // transitions
    if(counter > ie_transition) state = 21;
    else if (counter <= ie_transition) state = 11;
  }
}
void stop() {
  state = 0;
  counter = 0;
  compressor.writeMicroseconds(OFF_ESC_PW);
}
void start() {
  state = 11;
  counter = 0;
}
void update_parameters(int *new_r_rate, float *new_r_ratio) {
  noInterrupts();
  r_rate = *new_r_rate;
  r_ratio = *new_r_ratio;
  overflow = int(60000.0/r_rate-1);
  ie_transition = overflow * 1/r_ratio;
  interrupts();
}

// Define Handler Server Functions
void parametersGet(Request *req, Response *res) {
  sprintf(res->data, "{\"is_enabled\":%s,\"r_rate\":%d, \"r_ratio\":%f}", isEnabled() ? "true":"false", r_rate, r_ratio);
  res->status(200, "OK");
}
void parametersPost(Request *req, Response *res) {
  int req_r_rate = req->jsonInt("r_rate");
  float req_r_ratio = req->jsonFloat("r_ratio");

  if (req_r_rate > MAX_R_RATE || req_r_rate < MIN_R_RATE) {
    res->status(400, "Bad Resquest");
    return;
  }
  if (req_r_ratio > MAX_R_RATIO || req_r_ratio < MIN_R_RATIO) {
    res->status(400, "Bad Request");
    return;
  }
  update_parameters(&req_r_rate, &req_r_ratio);
  sprintf(res->data, "{\"r_rate\":%d, \"r_ratio\":%f}", r_rate, r_ratio);
  res->status(200, "OK");
}
void startGet(Request *req, Response *res) {
  start();
  sprintf(res->data, "{\"is_enabled\":%s}", isEnabled() ? "true":"false");
  res->status(200, "OK");
}
void stopGet(Request *req, Response *res) {
  stop();
  sprintf(res->data, "{\"is_enabled\":%s}", isEnabled() ? "true":"false");
  res->status(200, "OK");
}

// Setup Sequence
void setup() {
  // Hardware
  pinMode(LED_PIN, OUTPUT);
  compressor.attach(COMPRESSOR_PIN);
  compressor.writeMicroseconds(OFF_ESC_PW);

  // Show IP With LED
  // blinkIP(WiFi.localIP(), 1);
  Serial.begin(115200);
  Serial.println("Started");

  // Start Services
  api.use("/parameters", "GET", &parametersGet);
  api.use("/parameters", "POST", &parametersPost);
  api.use("/start", "GET", &startGet);
  api.use("/stop", "GET", &stopGet);
  api.allowCors("*");
  api.begin();
  update_parameters(&r_rate, &r_ratio);
  attachSystemInterrupt(SysInterrupt_SysTick, stepStateMachine);
}

// Application Main Loop
void loop() {
  api.next();
}