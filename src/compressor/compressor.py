import csv
import matplotlib.pyplot as plt
import numpy as np
from constants import p_atm
from math import pi, atan, cos, sin

class Fluid:
    def __init__(self, cp, R, gamma):
        self.cp = cp
        self.R = R
        self.gamma = gamma

class Compressor:
    # fluid conditions at different compressor states
    p_in: float # input pressure
    ps_in: float # input stagnation pressure
    T_in: float # input temperature
    Ts_in: float # input stagnation temperature
    q_in: float # input flow
    A_in: float # input area
    v_in: float # input average velocity
    rho_in: float # input fluid density

    p_out: float # output pressure
    ps_out: float # output stagnation pressure
    T_out: float # output temperature
    Ts_out: float # output stagnation temperature
    q_out: float # output flow
    A_out: float # output area
    v_out: float # output average velocity
    rho_out: float # output fluid density

    p_eye: float # eye pressure
    ps_eye: float # eye stagnation pressure
    T_eye: float # eye temperature
    Ts_eye: float # eye stagnation temperature
    q_eye: float # eye flow
    A_eye: float # eye area
    v_eye: float # eye average velocity
    rho_eye: float # eye fluid density

    p_tip: float # tip pressure
    ps_tip: float # tip stagnation pressure
    T_tip: float # tip temperature
    Ts_tip: float # tip stagnation temperature
    q_tip: float # tip flow
    A_tip: float # tip area
    v_tip: float # absolute velocity at tip
    rho_tip: float # tip fluid density

    p_di: float # tip pressure
    ps_di: float # tip stagnation pressure
    T_di: float # tip temperature
    Ts_di: float # tip stagnation temperature
    q_di: float # tip flow
    A_di: float # tip area
    v_di: float # absolute velocity at tip
    rho_di: float # tip fluid density

    # other state parameters
    omega: float # angular velocity
    m: float # mass flow rate
    W: float #work added to the fluid
    T: float #torque to drive the impeller

    #: float compressor properties
    sigma: float # compressor sigma factor
    beta2: float # vane angle at the tip of the impeller, relative to radial direction
    eta_c: float # compressor isoentropic efficiency
    eta_i: float # impeller isoentropic efficiency
    eta_d: float # diffuser isoentropic efficiency
    i_d_eye: float # impeller diameter at eye
    i_d_root: float # impeller diameter at root
    i_d: float # impeller diameter
    d_out: float # diameter at compressor output
    d_in: float # diameter at compressor input
    h_tip: float # vane height at tip
    r_max: float # impeller maximum radius
    omega_max_rpm: float # impeller maximum speed

    def __init__(self, fluid: Fluid):
        self._fluid = fluid

    def update_areas_from_diameters(self):
        self.A_eye = (self.i_d_eye ** 2 - self.i_d_root ** 2) * pi / 4
        self.A_in = self.d_in ** 2 * pi / 4
        self.A_out = self.d_out ** 2 * pi / 4
        self.A_tip = pi * self.i_d * self.h_tip
        self.A_di = (self.d_d_out ** 2 - self.d_d_in ** 2) * pi / 4

    def solve_for_rpm(self, rpm, output_flow):
        self.omega = rpm / 60 * 2 * pi
        self.q_out = output_flow
        self.update_areas_from_diameters()

        # increase in stagnation temperature and output parameters
        self.Ts_out = (self.omega * self.i_d / 2) ** 2 / self._fluid.cp + self.Ts_in
        self.ps_out = self.ps_in * (1 + self.eta_d*(self.Ts_out - self.Ts_in)/self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))
        self.v_out = self.q_out / self.A_out
        self.T_out = self.Ts_out - self.v_out ** 2 / 2 / self._fluid.cp
        self.p_out = self.ps_out * (self.T_out / self.Ts_out) ** (self._fluid.gamma / (self._fluid.gamma - 1))
        self.rho_out = self.p_out / self._fluid.R / self.T_out
        
        # mass flow
        self.m = self.q_out * self.rho_out

        # velocity and density at the input are unknown, iterative method
        error = 1
        self.rho_in = self.ps_in / self._fluid.R / self.Ts_in # approx
        while(error > 0.01):
            self.q_in = self.m / self.rho_in
            self.v_in = self.q_in / self.A_in
            self.T_in = self.Ts_in - self.v_in ** 2 / 2 / self._fluid.cp
            self.p_in = self.ps_in * (self.T_in / self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))
            temp = self.rho_in
            self.rho_in = self.p_in / self._fluid.R / self.T_in
            error = abs(temp - self.rho_in)
        
        # calculate again with final rho_in
        self.q_in = self.m / self.rho_in
        self.v_in = self.q_in / self.A_in
        self.T_in = self.Ts_in - self.v_in ** 2 / 2 / self._fluid.cp
        self.p_in = self.ps_in * (self.T_in / self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))

        # calculate power and torque required
        self.W = self.m * self._fluid.cp * (self.Ts_out - self.Ts_in)
        if(self.omega > 0 ):
            self.T = self.W / self.omega

    def solve_for_output_condition(self, p_output, q_output):
        self.p_out = p_output
        self.q_out = q_output
        self.update_areas_from_diameters()

        # stagnation pressure at the output
        self.v_out = self.q_out / self.A_out
        self.ps_out = self.p_out # initial guess
        error = 1
        while error > 0.01:
            # increase in stagnation temperature from input to output, adiabatic compression
            self.Ts_out = self.Ts_in * (1 + ((self.ps_out/self.ps_in) ** ((self._fluid.gamma - 1) / self._fluid.gamma) - 1)/self.eta_c)
            self.T_out = self.Ts_out - self.v_out ** 2 / 2 / self._fluid.cp

            # new stagnation pressure
            temp = self.ps_out
            self.ps_out = self.p_out * (self.Ts_out / self.T_out) ** (self._fluid.gamma / (self._fluid.gamma - 1))
            error = abs(1 - temp/self.ps_out)
        
        # calculate again with final ps_out
        self.Ts_out = self.Ts_in * (1 + ((self.ps_out/self.ps_in) ** ((self._fluid.gamma - 1) / self._fluid.gamma) - 1)/self.eta_c)
        self.T_out = self.Ts_out - self.v_out ** 2 / 2 / self._fluid.cp

        # mass flow
        self.rho_out = self.p_out / self._fluid.R / self.T_out
        self.m = self.q_out * self.rho_out

        # velocity and density at the input are unknown, iterative method
        error = 1
        self.rho_in = self.ps_in / self._fluid.R / self.Ts_in # approx
        while(error > 0.01):
            self.q_in = self.m / self.rho_in
            self.v_in = self.q_in / self.A_in
            self.T_in = self.Ts_in - self.v_in ** 2 / 2 / self._fluid.cp
            self.p_in = self.ps_in * (self.T_in / self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))
            temp = self.rho_in
            self.rho_in = self.p_in / self._fluid.R / self.T_in
            error = abs(temp - self.rho_in)
        
        # calculate again with final rho_in
        self.q_in = self.m / self.rho_in
        self.v_in = self.q_in / self.A_in
        self.T_in = self.Ts_in - self.v_in ** 2 / 2 / self._fluid.cp
        self.p_in = self.ps_in * (self.T_in / self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))

        # calculate speed and power required
        self.W = self.m * self._fluid.cp * (self.Ts_out - self.Ts_in)
        self.omega = (self.W / self.m) ** 0.5 * 2 / self.i_d
        self.T = self.W / self.omega

    def optimize_parameters_to_state(self):

        # parameters at the eye, assume isentropic
        self.rho_eye = self.rho_in #approx
        self.Ts_eye = self.Ts_in
        self.ps_eye = self.ps_in * (self.Ts_eye / self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))
        error = 1
        while(error > 0.01):
            self.q_eye = self.m / self.rho_eye
            self.v_eye = self.q_eye / self.A_eye
            self.T_eye = self.Ts_eye - self.v_eye ** 2 / 2 / self._fluid.cp
            self.p_eye = self.ps_eye * (self.T_eye / self.Ts_eye) ** (self._fluid.gamma / (self._fluid.gamma - 1))
            temp = self.rho_eye
            self.rho_eye = self.p_eye / self._fluid.R / self.T_eye
            error = abs(temp - self.rho_eye)
        
        # calculate one final time with last estimated rho value
        self.q_eye = self.m / self.rho_eye
        self.v_eye = self.q_eye / self.A_eye
        self.T_eye = self.Ts_eye - self.v_eye ** 2 / 2 / self._fluid.cp
        self.p_eye = self.ps_eye * (self.T_eye / self.Ts_eye) ** (self._fluid.gamma / (self._fluid.gamma - 1))

        # use the input velocity to calculate the vane angles at the eye and root
        self.v_eye_tip = self.omega * self.i_d_eye / 2
        self.v_eye_root = self.omega * self.i_d_root / 2
        self.beta_eye_tip = atan(self.v_eye/self.v_eye_tip)
        self.beta_eye_root = atan(self.v_eye/self.v_eye_root)

        # derive air velocity at impeller tip
        v_tip_tangential = self.omega * self.i_d / 2 * self.sigma
        v_tip_radial = self.v_eye # approx
        self.v_tip = (v_tip_radial ** 2 + v_tip_tangential ** 2) ** 0.5
        
        # derive tip parameters from input considering part of the loss at the impeller
        self.ps_tip = self.ps_in * (1 + self.eta_i * (self.Ts_out - self.Ts_in) / self.Ts_in) ** (self._fluid.gamma / (self._fluid.gamma - 1))
        self.Ts_tip = self.Ts_out
        self.T_tip = self.Ts_tip - self.v_tip ** 2 / 2 / self._fluid.cp
        self.p_tip = self.ps_tip * (self.T_tip / self.Ts_tip) ** (self._fluid.gamma / (self._fluid.gamma - 1))
        self.rho_tip = self.p_tip / self._fluid.R / self.T_tip

        # parameters at the diffuser
        self.Ts_di = self.Ts_out
        self.ps_di = self.ps_out * (self.Ts_di / self.Ts_out) ** (self._fluid.gamma / (self._fluid.gamma - 1))
        self.rho_di = self.m / self.v_tip / self.A_di #approx
        error = 1
        while(error > 0.01):
            self.q_di = self.m / self.rho_di
            self.v_di = self.q_di / self.A_di
            self.T_di = self.Ts_di - self.v_di ** 2 / 2 / self._fluid.cp
            self.p_di = self.ps_di * (self.T_di / self.Ts_di) ** (self._fluid.gamma / (self._fluid.gamma - 1))
            temp = self.rho_di
            self.rho_di = self.p_di / self._fluid.R / self.T_di
            error = abs(temp - self.rho_di)
        
        # last calculation with best rho estimate
        self.q_di = self.m / self.rho_di
        self.v_di = self.q_di / self.A_di
        self.T_di = self.Ts_di - self.v_di ** 2 / 2 / self._fluid.cp
        self.p_di = self.ps_di * (self.T_di / self.Ts_di) ** (self._fluid.gamma / (self._fluid.gamma - 1))

        # optimum number of vanes
        ## Number of Vanes
        self.n_vanes = (cos(self.beta2) ** 0.5 / (1 - self.sigma)) ** (1/0.7)

    def generate_radial_involute(self, file_path: str):
        t = 0
        y = 1
        while y > 0:
            t = t+0.001
            y = cos(t)*(sin(self.beta2) - t*self.i_d_root/2/self.i_d/2 + t*cos(self.beta2)) + sin(t)*(t*sin(self.beta2) + self.i_d_root/2/self.i_d/2 - cos(self.beta2)) - sin(self.beta2)
        involute_radius = self.i_d/2*sin(self.beta2)/(sin(t)-t*cos(t))
        l = involute_radius - self.i_d_root/2
        with open(file_path, 'w') as f:
            filewriter = csv.writer(f, delimiter=',')
            for i in range(12):
                a = i*t/10
                y = -involute_radius*(sin(a) - a*cos(a)) 
                x = involute_radius*(cos(a) + a*sin(a)) - involute_radius + self.i_d_root/2
                z = 0
                filewriter.writerow([x*100,y*100,z*100])

    def print_parameters(self):
        print("-> Parameters")
        if self.beta_eye_tip:
            print(f"Beta Eye Tip: {self.beta_eye_tip * 180 / pi} deg")
        if self.beta_eye_root:
            print(f"Beta Eye Root: {self.beta_eye_root * 180 / pi} deg")
        print(f"Number of Vanes {self.n_vanes}")

    def print_state(self):
        print(f"-> Mass flow: {self.m} kg/s")
        print(f"-> Output flow: {self.q_out}")
        print(f"-> Power: {self.W} W")
        print(f"-> Torque: {self.T} Nm")
        print(f"-> Omega: {self.omega / 2 / pi * 60} RPM or {self.omega} rad/s\n")
        print("-> Input:")
        print(f"v: {self.v_in} m/s")
        print(f"A: {self.A_in*1e6} mm2")
        print(f"p: {self.p_in} Pa")
        print(f"ps: {self.ps_in} Pa")
        print(f"T: {self.T_in} K")
        print(f"Ts: {self.Ts_in} K")
        print(f"rho: {self.rho_in} kg/m3\n")
        print("-> Output:")
        print(f"v: {self.v_out} m/s")
        print(f"A: {self.A_out*1e6} mm2")
        print(f"p: {self.p_out} Pa")
        print(f"ps: {self.ps_out} Pa")
        print(f"T: {self.T_out} K")
        print(f"Ts: {self.Ts_out} K")
        print(f"rho: {self.rho_out} kg/m3\n")
        if self.v_eye:
            print("-> Eye:")
            print(f"v: {self.v_eye} m/s")
            print(f"A: {self.A_eye*1e6} mm2")
            print(f"p: {self.p_eye} Pa")
            print(f"ps: {self.ps_eye} Pa")
            print(f"T: {self.T_eye} K")
            print(f"Ts: {self.Ts_eye} K")
            print(f"rho: {self.rho_eye} kg/m3\n")
        if self.v_tip:
            print("-> Tip:")
            print(f"v: {self.v_tip} m/s")
            print(f"A: {self.A_tip*1e6} mm2")
            print(f"p: {self.p_tip} Pa")
            print(f"ps: {self.ps_tip} Pa")
            print(f"T: {self.T_tip} K")
            print(f"Ts: {self.Ts_tip} K")
            print(f"rho: {self.rho_tip} kg/m3\n")
        if self.v_di:
            print("-> Diffuser:")
            print(f"v: {self.v_di} m/s")
            print(f"A: {self.A_di*1e6} mm2")
            print(f"p: {self.p_di} Pa")
            print(f"ps: {self.ps_di} Pa")
            print(f"T: {self.T_di} K")
            print(f"Ts: {self.Ts_di} K")
            print(f"rho: {self.rho_di} kg/m3\n")

    def plot_pressure_as_f_of_omega(self, q_output):
        n = 10
        rpm_array = np.linspace(0, self.omega*60 / 2 / pi * 1.2, n)
        pressure_array = np.zeros(rpm_array.shape)
        for i, rpm in enumerate(rpm_array):
            self.solve_for_rpm(rpm, q_output)
            pressure_array[i] = self.p_out - self.p_in

        fig, ax1 = plt.subplots()
        plt.title("Compressor Pressure for Angular Speed")
        ax1.set_xlabel('RPM')
        ax1.set_ylabel('Pressure (Pa)')
        ax1.plot(rpm_array, pressure_array)
        ax1.tick_params(axis='y')
        plt.grid()
        plt.show()
