rho_h2o = 1000 # water specific mass
p_atm = 84556 # atmospheric pressure
T_atm = 293.15 # atmospheric temperature
g = 9.80665 # standard gravity acceleration