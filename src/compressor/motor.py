class Motor:

    @staticmethod
    def estimateKv(torque: float, omega: float, voltage: float, resistance: float) -> float:
        # kt solution is a quadratic equation
        a = omega
        b = -voltage
        c = resistance*torque
        if (4*a*c > b**2): raise Exception("Could not solve KV estimation, complex roots")
        
        # roots
        k1 = (-b + (b**2 - 4*a*c)**0.5) / 2 / a
        k2 = (-b - (b**2 - 4*a*c)**0.5) / 2 / a

        # convert kt to kv
        k1 = 8.3/k1
        k2 = 8.3/k2
        return k1 # first one has physical meaning

    def __init__(self, P_max: float, kv: float, i_max: float, R: float, V_max: float):
        self.P_max = P_max
        self.kv = kv
        self.kt = 8.3/kv
        self.R = R
        self.i_max = i_max
        self.V_max = V_max

    def stateString(self):
        return f"operating at {self.i:.2f}A and {self.V:.2f}V"

    def deriveStateFromToqueAndOmega(self, torque:float, safetyFactor: float, omega: float):
        self.i = torque * safetyFactor / self.kt
        self.V = self.i * self.R + omega * self.kt
        self.P = self.i * self.V

    def objectiveFunction(self):
        # goal is to minimize function
        V_factor = 0 if self.V < self.V_max else 1
        i_factor = 0 if self.i < self.i_max else 1
        P_factor = 0 if self.P < self.P_max else 1
        iLoading_factor = self.i / self.i_max
        return 1*V_factor + 1*i_factor + 1*P_factor + 1*iLoading_factor

