### Compressor Design Calculations
### variables without unit definitions are in SI units
import numpy as np
import matplotlib.pyplot as plt

from compressor import Compressor, Fluid
from motor import Motor
from constants import p_atm, T_atm, g, rho_h2o
from math import pi
from os import path
from typing import Dict

## Input Requirements
max_pressure_mmH2O = 800
flow_at_max_pressure = 300e-6

## Air and Oxygen Fluids
air = Fluid(cp=1006, R=287.05, gamma=1.4)
oxygen = Fluid(cp=918, R=259.84, gamma=1.4)

## Compressor Object
compressor = Compressor(air)

# characteristics
compressor.sigma = 0.9 # slip factor between the impeller and the fluid
compressor.beta2 = -66 * pi / 180 # vane angle at the tip of the impeller, relative to radial direction
compressor.eta_c = 0.7 # compressor isoentropic efficiency
compressor.eta_i = 0.8 # isoentropic efficiency at the impeller
compressor.eta_d = 0.8 # isoentropic efficiency at the diffuser
compressor.i_d_eye = 16e-3 # impeller diameter at eye
compressor.i_d_root = 8e-3 # impeller diameter at root
compressor.i_d = 64e-3 # impeller diameter
compressor.d_out = 16e-3 # diameter at compressor output
compressor.d_in = 16e-3 # diameter at compressor input
compressor.h_tip = 3e-3 # height of channel at impeller tip
compressor.d_d_out = 72e-3 # outer diameter of diffuser
compressor.d_d_in = 68e-3 # outer diameter of diffuser

# limits
compressor.r_max = 65e-3/2 # impeller maximum radius
compressor.omega_max_rpm = 55000 # impeller maximum speed

# conditions at the inlet
p_stagnation_in = p_atm
T_stagnation_in = T_atm
compressor.ps_in = p_stagnation_in
compressor.Ts_in = T_stagnation_in

# desired output conditions
p_output = p_atm + max_pressure_mmH2O / 1000 * rho_h2o * g
q_output = flow_at_max_pressure

# print operating state parameters for the maximum flow and pressure
compressor.solve_for_output_condition(p_output, q_output)
compressor.optimize_parameters_to_state()
compressor.print_state()
compressor.print_parameters()

# create involute curve to import into CAD
file_path = path.join(path.dirname(path.abspath(__file__)), "radial_involute.csv")
compressor.generate_radial_involute(file_path)

# plot curve for compressor
compressor.plot_pressure_as_f_of_omega(flow_at_max_pressure)


## Motor Selection
# estimate motor Kv
compressor.solve_for_output_condition(p_output, q_output)
optimalKv_02 = Motor.estimateKv(compressor.T, compressor.omega, 3.6, 0.2)
optimalKv_06 = Motor.estimateKv(compressor.T, compressor.omega, 3.6, 0.6)
print("\n-> Motor Selection")
print(f"Optimal Kv for 3.6V between {optimalKv_02:.2f} and {optimalKv_06:.2f} RPM/V")
optimalKv_02 = Motor.estimateKv(compressor.T, compressor.omega, 7.2, 0.2)
optimalKv_06 = Motor.estimateKv(compressor.T, compressor.omega, 7.2, 0.6)
print(f"Optimal Kv for 7.2V between {optimalKv_02:.2f} and {optimalKv_06:.2f} RPM/V")

# available motors
motorList: Dict[str, Motor] = {
    "A": Motor(P_max=9.6, kv=5750, i_max=1.2, R=0.6, V_max=7.4),
    "B": Motor(P_max=40, kv=8000, i_max=5.46, R=0.22, V_max=12),
    "C": Motor(P_max=80, kv=9500, i_max=10, R=0.141, V_max=7.4),
    "D": Motor(P_max=130, kv=5300, i_max=18, R=0.031, V_max=7.4),
    "E": Motor(P_max=45, kv=6100, i_max=5, R=0.029, V_max=7.4),
    "F": Motor(P_max=45, kv=4300, i_max=5, R=0.056, V_max=7.4),
    "G": Motor(P_max=35, kv=4200, i_max=5, R=0.3, V_max=7.4),
    "H": Motor(P_max=270, kv=4200, i_max=22,R=0.4, V_max=12),
}

# set operating state of motors to max compressor stress
safetyFactor = 2
print("Best motor in descending order")
for key, motor in motorList.items(): motor.deriveStateFromToqueAndOmega(compressor.T, safetyFactor, compressor.omega)
score = {k:motor.objectiveFunction() for k,motor in motorList.items()}
[print(f" Motor {k} at {motorList[k].i:.2e}A and {motorList[k].V:.2f}V with a score of {v:.1e}") for k,v in sorted(score.items(), key=lambda x: x[1])]
