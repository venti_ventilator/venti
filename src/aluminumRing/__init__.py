from math import pi
from src.lib import Component

# class AluminumBar(OffTheShelf):
#     self.__init__(self):
#         OffTheShelf.__init__(self)
#         self.manufacturer = McMaster
#         self.manufacturer_part_number = "X"

class AluminumRing(Component):
    alpha = pi/2

    def __init__(self):
        Component.__init__(self)
        self.update()

    @property
    def out_width(self):
        return self._out_width
    
    @out_width.setter
    def out_width(self, value):
        self._out_width = value
        self.update()
    
    @property
    def thickness(self):
        return self._thickness
    
    @thickness.setter
    def thickness(self, value):
        self._thickness = value
        self.update()
    
    @property
    def out_height(self):
        return self._out_height
    
    @out_height.setter
    def out_height(self, value):
        self._out_height = value
        self.update()

    @property
    def inner_radius(self):
        return self._inner_radius
    
    @inner_radius.setter
    def inner_radius(self, value):
        self.inner_radius = value
        self.update()

    @property
    def material_k(self):
        return self._k
    
    @material_k.setter
    def material_k(self, value):
        self._k = value
        self.update

    @property
    def elastic_strain_limit(self):
        return self._elastic_strain_limit
    
    @elastic_strain_limit.setter
    def elastic_strain_limit(self, value):
        self._elastic_strain_limit = value
        self.update()

    @property
    def tray_hole_height(self):
        return self._tray_hole_height

    @tray_hole_height.setter
    def tray_hole_height(self, value):
        self._tray_hole_height
        self.update()

    @property
    def top_hole_distance(self):
        return self._top_hole_distance

    @top_hole_distance.setter
    def top_hole_distance(self, value):
        self._top_hole_distance = value
        self.update()

    def update(self):
        arc_length_flat = self.alpha * (self._inner_radius + self._thickness * self._k)
        self.bend_to = 2 * self.alpha * self._elastic_strain_limit * (self._inner_radius / self._k / self._thickness + 1)
        self.a = 0
        self.b = self._out_width / 2 - self._thickness - self._inner_radius
        self.c = self.b + arc_length_flat
        self.d = self.c + self._tray_hole_height - self._inner_radius - self._thickness
        self.e = self.c + self._out_height - 2 * (self._inner_radius + self._thickness)
        self.f = self.e + arc_length_flat
        self.i = self.f + self._out_width - 2 * (self._inner_radius + self._thickness)
        temp = (self.i - self.f) / 2 + self.f
        self.g = temp - self._top_hole_distance / 2
        self.h = temp + self._top_hole_distance / 2
        self.j = self.i + arc_length_flat
        self.l = self.j + self._out_height - 2 * (self._inner_radius + self._thickness)
        self.k = self.l - self._tray_hole_height + self._inner_radius + self._thickness
        self.m = self.l + arc_length_flat
        self.n = self.m + self.b

    def __str__(self):
        decimals = 2
        return f"""
        Bend to {self.bend_to * 180 / pi} degrees,
        In millimeters:
        a = {self.a*1e3:.{decimals}f}
        b = {self.b*1e3:.{decimals}f}
        c = {self.c*1e3:.{decimals}f}
        d = {self.d*1e3:.{decimals}f}
        e = {self.e*1e3:.{decimals}f}
        f = {self.f*1e3:.{decimals}f}
        g = {self.g*1e3:.{decimals}f}
        h = {self.h*1e3:.{decimals}f}
        i = {self.i*1e3:.{decimals}f}
        j = {self.j*1e3:.{decimals}f}
        k = {self.k*1e3:.{decimals}f}
        l = {self.l*1e3:.{decimals}f}
        m = {self.m*1e3:.{decimals}f}
        n = {self.n*1e3:.{decimals}f}
        """

    def manufacture(self):
        decimals = 2
        return f"""
# Manufacturing Process of Aluminum Ring

## Summary
Total Time: 1 h
Total Cost: USD $10

## Parts
 - `Aluminum Bar`, McMaster, 8975K577, 1.62

## Tools
 - 3/8" Drill
 - Fine Marking Punch
 - Hammer
 - 3.5mm HSS Drill Bit
 - 2.5mm HSS Drill Bit
 - M3x0.5 HSS Tap
 - M4x0.5 HSS Tap
 - Tap Holder
 - `Flat Stock Bender` with 1-1/2" die
 - Caliper
 - Propane Torch
 - Aluminum Brazing Rod
 - Sand Paper
 - File

## Diagram
![Diagram](/src/aluminumRing/diagram.png)

## Steps
1. (15min) Mark the `Aluminum Bar` at the following lengths:
   - b: {self.b*1e3:.{decimals}f} mm
   - c: {self.c*1e3:.{decimals}f} mm
   - e: {self.e*1e3:.{decimals}f} mm
   - f: {self.f*1e3:.{decimals}f} mm
   - i: {self.i*1e3:.{decimals}f} mm
   - j: {self.j*1e3:.{decimals}f} mm
   - l: {self.l*1e3:.{decimals}f} mm
   - m: {self.m*1e3:.{decimals}f} mm
   - n: {self.n*1e3:.{decimals}f} mm
2. (15min) Mark and Punch at the following lengths:
   - d: {self.d*1e3:.{decimals}f} mm
   - g: {self.g*1e3:.{decimals}f} mm
   - h: {self.h*1e3:.{decimals}f} mm
   - k: {self.k*1e3:.{decimals}f} mm
3. (Xmin) Drill with 2.5 mm bit and tap a M3x0.5 thread at: g, h.
4. (Xmin) Drill with 3.5 mm bit and tap a M4x0.5 thread at: d, k.
5. (Xmin) Cut at length {self.n*1e3:.{decimals}f} mm.
6. (Xmin) Assemble `Flat Stock Bender` with 1-1/2" die.
7. (Xmin) Bend to a {self.bend_to * 180 / pi:.{decimals}f} degrees angle at the lengths: b, e, i and l.
8. (Xmin) Braze bottom to close the loop.
9. (Xmin) Finish surface with File and Sand Paper.
"""