
# Manufacturing Process of Aluminum Ring

## Summary
Total Time: 1 h
Total Cost: USD $10

## Parts
 - `Aluminum Bar`, McMaster, 8975K577, 1.62

## Tools
 - 3/8" Drill
 - Fine Marking Punch
 - Hammer
 - 3.5mm HSS Drill Bit
 - 2.5mm HSS Drill Bit
 - M3x0.5 HSS Tap
 - M4x0.5 HSS Tap
 - Tap Holder
 - `Flat Stock Bender`
 - Caliper
 - Propane Torch
 - Aluminum Brazing Rod
 - Sand Paper
 - File

## Diagram
![Diagram](/src/aluminumRing/diagram.png)

## Steps
1. (15min) Mark the `Aluminum Bar` at the following lengths:
   - b: 51.08 mm
   - c: 83.39 mm
   - e: 178.32 mm
   - f: 210.64 mm
   - i: 312.79 mm
   - j: 345.11 mm
   - l: 440.04 mm
   - m: 472.36 mm
   - n: 523.44 mm
2. (15min) Mark and Punch at the following lengths:
   - d: 99.47 mm
   - g: 226.72 mm
   - h: 296.72 mm
   - k: 423.97 mm
3. (Xmin) Drill with 2.5 mm bit and tap a M3x0.5 thread at: g, h.
4. (Xmin) Drill with 3.5 mm bit and tap a M4x0.5 thread at: d, k.
5. (Xmin) Cut at length 523.44 mm.
6. (Xmin) Assemble `Flat Stock Bender` with 3/4in die.
7. (Xmin) Bend to a 9.73 degrees angle at the lengths: b, e, i and l.
8. (Xmin) Braze bottom to close the loop.
9. (Xmin) Finish surface with File and Sand Paper.
