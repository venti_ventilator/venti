from os import path
from abc import abstractmethod
from json import loads
import inspect

class Component:

    def __init__(self):
        # load properties file if exists
        path_to_properties = path.join(path.dirname(inspect.getfile(self.__class__)), "properties.json")
        if path.isfile(path_to_properties):
            with open(path_to_properties, 'r') as f:
                props = loads(f.read())
                for k, v in props.items():
                    self.__dict__[k] = v
    
    def build_doc(self):
        path_to_manufacture = path.join(path.dirname(inspect.getfile(self.__class__)), "manufacture.md")
        with open(path_to_manufacture, 'w') as f:
            f.write(self.manufacture())

    @abstractmethod
    def manufacture(self):
        pass